<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'description'];
    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
