import React,{Component} from 'react'
class NewProject extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            errors: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.hasError = this.hasError.bind(this);
        this.renderError = this.renderError.bind(this);
    }
    handleChange(event){
        this.setState({
            [event.target.name] : event.target.value
        })
    }
    handleSubmit(event){
        event.preventDefault();
        const { history } = this.props
        const project = {
            name: this.state.name,
            description: this.state.description
        }
        axios.post('/api/projects',project).then(response => {
            console.log(response);
            if(response.data.status === 0){
                this.setState({
                    errors: {name:["The name field is required."]}
                });
            }else{
                history.push('/');
            }
        }).catch(error => {
            console.log(error);
        })
    }
    hasError(field){
        return !!this.state.errors[field]
    }
    renderError(field){
        console.log(this.state.errors);
        if(this.state.errors[field]){
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    }
    render() {
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header'>Create new project</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleSubmit}>
                                    <div className='form-group'>
                                        <label htmlFor='name'>Project name</label>
                                        <input
                                            id='name'
                                            type='text'
                                            className={`form-control ${this.hasError('name') ? 'is-invalid' : ''}`}
                                            name='name'
                                            value={this.state.name}
                                            onChange={this.handleChange}
                                        />
                                        {this.renderError('name')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='description'>Project description</label>
                                        <textarea
                                            id='description'
                                            className={`form-control ${this.hasError('description') ? 'is-invalid' : ''}`}
                                            name='description'
                                            rows='10'
                                            value={this.state.description}
                                            onChange={this.handleChange}
                                        />
                                        {this.renderError('description')}
                                    </div>
                                    <button className='btn btn-primary'>Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default NewProject;
