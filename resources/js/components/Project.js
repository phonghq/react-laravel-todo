import React, { Component } from 'react';

class Project extends Component{
    constructor(props) {
        super(props);
        this.state = {
            project : {},
            tasks : [],
            title: '',
            errors: [],
        }
        this.handleMarkProjectCompleted = this.handleMarkProjectCompleted.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCreateTask = this.handleCreateTask.bind(this)
    }
    handleChange(event){
        this.setState({
            title: event.target.value
        })
    }
    handleCreateTask(event){
        event.preventDefault();
        const task = {
            title : this.state.title,
            project_id: this.state.project.id
        }
        axios.post('/api/tasks',task).then(response =>{
            this.setState({
                title: '',
            });
            this.setState(prevState => ({
                tasks : prevState.tasks.concat(response.data.data)
            }))
        })
    }
    componentDidMount() {
        const projectId = this.props.match.params.id;
        axios.get(`/api/projects/${projectId}`).then(response =>{
            console.log(response.data.data);
            this.setState({
                project:response.data.data,
                tasks:response.data.data.tasks
            })
        })
    }
    handleMarkProjectCompleted(){
        axios.post(`/api/projects/${this.state.project.id}`).then(response =>{
            this.props.history.push('/');
        })
    }
    handleMarkTaskComplete(taskId){
        axios.post(`/api/tasks/${taskId}`).then(response =>{
            window.location.reload(false);
            //xoá task đã hoàn thành
            // this.setState(prevState => ({
            //     task : prevState.tasks.filter(task => {
            //         return task.id !== taskId;
            //     })
            // }))
        })
    }

    hasError(field){
        return !!this.state.errors[field]
    }
    renderError(field){
        console.log(this.state.errors);
        if(this.state.errors[field]){
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    }
    renderMarkComplete(task){
        if(task.is_completed === 1){
            return (
                <span className="badge badge-success">Completed</span>
            )
        }
        return (
            <button className='btn btn-primary btn-sm'
                    onClick={this.handleMarkTaskComplete.bind(this,task.id)}>
                Mark as completed
            </button>
        );
    }
    render() {
        const {project, tasks} = this.state;
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header'>{project.name} AAAAA</div>
                            <div className='card-body'>
                                <p>{project.description}</p>

                                <button onClick={this.handleMarkProjectCompleted} className='btn btn-primary btn-sm'>
                                    Mark as completed
                                </button>

                                <hr />
                                <form onSubmit={this.handleCreateTask}>
                                    <div className='input-group'>
                                        <input
                                            type='text'
                                            name='title'
                                            className={`form-control ${this.renderError('title') ? 'is-invalid' : ''}`}
                                            placeholder='Task title'
                                            value={this.state.title}
                                            onChange={this.handleChange}
                                        />
                                        <div className='input-group-append'>
                                            <button className='btn btn-primary'>Add</button>
                                        </div>
                                        {this.renderError('title')}
                                    </div>
                                </form>
                                <ul className='list-group mt-3'>
                                    {tasks.map(task => (
                                        <li
                                            className='list-group-item d-flex justify-content-between align-items-center'
                                            key={task.id}
                                        >
                                            {task.title}
                                            {this.renderMarkComplete(task)}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Project;
