import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
    <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
        <div className='container'>
            <Link className='navbar-brand' to='/'>ToDo</Link>
            <div className="flex-center position-ref full-height">
                <div className="top-right links">
                    <a>Login</a>
                    <a>Register</a>
                </div>
            </div>
        </div>
    </nav>
);
export default Header;
