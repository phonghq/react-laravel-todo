<?php

namespace App\Http\Controllers;

use App\Model\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    public function index(Request $request){
        $projects = Project::where('is_completed',false)
                        ->orderBy('created_at', 'desc')
                        ->withCount(['tasks' => function ($query) {
                                $query->where('is_completed', false);
                        }])->get();
        return response()->json(['data' => $projects,'status'=>1],200);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['status'=>0,'message'=>'Điền thông tin tên project'],200);
        }
        $data = $request->all();
        try {
            DB::beginTransaction();
            $project = Project::create([
                'name' => $data['name'],
                'description' => $data['description'],
            ]);
            DB::commit();
            return response()->json(['status'=>1,'message'=>'Project is created successfull'],200);
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e);
        }
    }
    public function show($id){
        $project = Project::with('tasks')->find($id);
        $status = isset($project) ? 1 : 0;
        return response()->json(['status'=>$status,'data'=>$project],200);
    }
    public function markAsCompleted($id)
    {
        try {
            DB::beginTransaction();
            $project = Project::find($id);
            $project->is_completed = true;
            $project->update();
            DB::commit();
            return response()->json('Project updated!');
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e);
        }
    }
}
