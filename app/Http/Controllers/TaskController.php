<?php

namespace App\Http\Controllers;

use App\Model\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['status'=>0,'message'=>'Điền thông tin tên task'],200);
        }
        $data = $request->all();
        try {
            DB::beginTransaction();
            $task = Task::create([
                'title' => $data['title'],
                'project_id' => $data['project_id'],
            ]);
            DB::commit();
            return response()->json(['status'=>1,'message'=>'Task is created','data'=>$task],200);
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e);
        }
    }

    public function markAsCompleted($id)
    {
        try {
            DB::beginTransaction();
            $task = Task::where('is_completed',false)->where('id',$id)->first();
            $task->is_completed = true;
            $task->update();
            DB::commit();
            return response()->json(['status'=>1,'message'=>'Task is updated'],200);
        }catch (\Exception $e){
            DB::rollBack();
            throwException($e);
            return response()->json(['status'=>0,'message'=>'Something went wrong'],200);
        }


    }
}
